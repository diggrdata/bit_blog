class Post < ActiveRecord::Base
#relationship to comments added, plus destroy
 has_many :comments, dependent: :destroy
#validation rules added
 validates_presence_of :title
 validates_presence_of :body
end

