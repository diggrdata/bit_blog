class Comment < ActiveRecord::Base
#added relationship to post
 belongs_to :post
#added validation
 validates_presence_of :post_id
 validates_presence_of :body
end
